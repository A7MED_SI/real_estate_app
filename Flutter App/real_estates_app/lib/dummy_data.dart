import './models/post.dart';

var dummyRecent = [
  Post(
    date: DateTime.now(),
    likesCount: 14,
    // dislikesCount: 14,
    id: 1,
    mainImageUrl: 'assets/images/13.jpg',
    location: 'Boston - 1234 street',
    description:
        'dkafjkajfkajflakjflkjfklaj kajfkjaflkj akdjfakjfj aldkjf ajldkjf aklj kjl ajkdj lajfkjd kjdalkjf kajdkljf kajlfj ',
    imageUrls: [
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
    ],
    saleOrRent: 'For Sale',
    // userName: 'A7MED SI',
  ),
  Post(
    date: DateTime.now(),
    // dislikesCount: 14,
    likesCount: 11,
    id: 2,
    mainImageUrl: 'assets/images/17.jpg',
    location: 'Miami - 1334 street',
    // userName: 'MOHAMMED',
    description:
        'dkafjkajfkajflakjflkjfklaj kajfkjaflkj akdjfakjfj aldkjf ajldkjf aklj kjl ajkdj lajfkjd kjdalkjf kajdkljf kajlfj ',
    imageUrls: [
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
    ],
    saleOrRent: 'For Sale',
  ),
  Post(
    date: DateTime.now(),
    // dislikesCount: 14,
    likesCount: 13,
    id: 3,
    mainImageUrl: 'assets/images/16.jpg',
    location: 'NewYork - 14 street',
    // userName: 'YOUSEF',
    description:
        'dkafjkajfkajflakjflkjfklaj kajfkjaflkj akdjfakjfj aldkjf ajldkjf aklj kjl ajkdj lajfkjd kjdalkjf kajdkljf kajlfj ',
    imageUrls: [
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
    ],
    saleOrRent: 'For Sale',
  ),
  Post(
    date: DateTime.now(),
    // dislikesCount: 14,
    likesCount: 3,
    id: 4,
    mainImageUrl: 'assets/images/14.jpg',
    location: 'Boston - 1234 street',
    // userName: 'HAITHAM',
    description:
        'dkafjkajfkajflakjflkjfklaj kajfkjaflkj akdjfakjfj aldkjf ajldkjf aklj kjl ajkdj lajfkjd kjdalkjf kajdkljf kajlfj ',
    imageUrls: [
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
    ],
    saleOrRent: 'For Sale',
  ),
  Post(
    date: DateTime.now(),
    // dislikesCount: 14,
    likesCount: 154,
    id: 5,
    mainImageUrl: 'assets/images/15.jpg',
    location: 'Boston - 1234 street',
    // userName: 'BRAYAN',
    description:
        'dkafjkajfkajflakjflkjfklaj kajfkjaflkj akdjfakjfj aldkjf ajldkjf aklj kjl ajkdj lajfkjd kjdalkjf kajdkljf kajlfj ',
    imageUrls: [
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
    ],
    saleOrRent: 'For Sale',
  ),
  Post(
    date: DateTime.now(),
    // dislikesCount: 14,
    likesCount: 124,
    id: 6,
    mainImageUrl: 'assets/images/13.jpg',
    location: 'Boston - 1234 street',
    // userName: 'MICHEALE',
    description:
        'dkafjkajfkajflakjflkjfklaj kajfkjaflkj akdjfakjfj aldkjf ajldkjf aklj kjl ajkdj lajfkjd kjdalkjf kajdkljf kajlfj ',
    imageUrls: [
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
    ],
    saleOrRent: 'For Sale',
  ),
  Post(
    date: DateTime.now(),
    // dislikesCount: 14,
    likesCount: 111,
    id: 7,
    mainImageUrl: 'assets/images/22.jpg',
    location: 'Boston - 1234 street',
    // userName: 'SHAHED',
    description:
        'dkafjkajfkajflakjflkjfklaj kajfkjaflkj akdjfakjfj aldkjf ajldkjf aklj kjl ajkdj lajfkjd kjdalkjf kajdkljf kajlfj ',
    imageUrls: [
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
    ],
    saleOrRent: 'For Sale',
  ),
  Post(
    date: DateTime.now(),
    // dislikesCount: 14,
    likesCount: 14,
    id: 8,
    mainImageUrl: 'assets/images/21.jpg',
    location: 'Boston - 1234 street',
    // userName: 'ELIZABITH',
    description:
        'dkafjkajfkajflakjflkjfklaj kajfkjaflkj akdjfakjfj aldkjf ajldkjf aklj kjl ajkdj lajfkjd kjdalkjf kajdkljf kajlfj ',
    imageUrls: [
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
    ],
    saleOrRent: 'For Sale',
  ),
];

var dummyMostLikes = [
  Post(
    date: DateTime.now(),
    // dislikesCount: 14,
    likesCount: 14,
    id: 1,
    mainImageUrl: 'assets/images/22.jpg',
    location: 'Boston - 1234 street',
    // userName: 'A7MED SI'
    description:
        'dkafjkajfkajflakjflkjfklaj kajfkjaflkj akdjfakjfj aldkjf ajldkjf aklj kjl ajkdj lajfkjd kjdalkjf kajdkljf kajlfj ',
    imageUrls: [
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
    ],
    saleOrRent: 'For Sale',
  ),
  Post(
    date: DateTime.now(),
    // dislikesCount: 14,
    likesCount: 11,
    id: 2,
    mainImageUrl: 'assets/images/13.jpg',
    location: 'Miami - 1334 street',
    // userName: 'MOHAMMED'
    description:
        'dkafjkajfkajflakjflkjfklaj kajfkjaflkj akdjfakjfj aldkjf ajldkjf aklj kjl ajkdj lajfkjd kjdalkjf kajdkljf kajlfj ',
    imageUrls: [
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
    ],
    saleOrRent: 'For Sale',
  ),
  Post(
    date: DateTime.now(),
    // dislikesCount: 14,
    likesCount: 13,
    id: 3,
    mainImageUrl: 'assets/images/14.jpg',
    location: 'NewYork - 14 street',
    // userName: 'YOUSEF'
    description:
        'dkafjkajfkajflakjflkjfklaj kajfkjaflkj akdjfakjfj aldkjf ajldkjf aklj kjl ajkdj lajfkjd kjdalkjf kajdkljf kajlfj ',
    imageUrls: [
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
    ],
    saleOrRent: 'For Sale',
  ),
  Post(
    date: DateTime.now(),
    // dislikesCount: 14,
    likesCount: 3,
    id: 4,
    mainImageUrl: 'assets/images/19.jpg',
    location: 'Boston - 1234 street',
    // userName: 'HAITHAM'
    description:
        'dkafjkajfkajflakjflkjfklaj kajfkjaflkj akdjfakjfj aldkjf ajldkjf aklj kjl ajkdj lajfkjd kjdalkjf kajdkljf kajlfj ',
    imageUrls: [
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
    ],
    saleOrRent: 'For Sale',
  ),
  Post(
    date: DateTime.now(),
    // dislikesCount: 14,
    likesCount: 154,
    id: 5,
    mainImageUrl: 'assets/images/23.jpg',
    location: 'Boston - 1234 street',
    // userName: 'BRAYAN'
    description:
        'dkafjkajfkajflakjflkjfklaj kajfkjaflkj akdjfakjfj aldkjf ajldkjf aklj kjl ajkdj lajfkjd kjdalkjf kajdkljf kajlfj ',
    imageUrls: [
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
    ],
    saleOrRent: 'For Sale',
  ),
  Post(
    date: DateTime.now(),
    // dislikesCount: 14,
    likesCount: 124,
    id: 6,
    mainImageUrl: 'assets/images/12.jpg',
    location: 'Boston - 1234 street',
    // userName: 'MICHEALE'
    description:
        'dkafjkajfkajflakjflkjfklaj kajfkjaflkj akdjfakjfj aldkjf ajldkjf aklj kjl ajkdj lajfkjd kjdalkjf kajdkljf kajlfj ',
    imageUrls: [
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
    ],
    saleOrRent: 'For Sale',
  ),
  Post(
    date: DateTime.now(),
    // dislikesCount: 14,
    likesCount: 111,
    id: 7,
    mainImageUrl: 'assets/images/22.jpg',
    location: 'Boston - 1234 street',
    // userName: 'SHAHED'
    description:
        'dkafjkajfkajflakjflkjfklaj kajfkjaflkj akdjfakjfj aldkjf ajldkjf aklj kjl ajkdj lajfkjd kjdalkjf kajdkljf kajlfj ',
    imageUrls: [
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
    ],
    saleOrRent: 'For Sale',
  ),
  Post(
    date: DateTime.now(),
    // dislikesCount: 14,
    likesCount: 14,
    id: 8,
    mainImageUrl: 'assets/images/21.jpg',
    location: 'Boston - 1234 street',
    // userName: 'ELIZABITH'
    description:
        'dkafjkajfkajflakjflkjfklaj kajfkjaflkj akdjfakjfj aldkjf ajldkjf aklj kjl ajkdj lajfkjd kjdalkjf kajdkljf kajlfj ',
    imageUrls: [
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
      'assets/images/18.jpg',
    ],
    saleOrRent: 'For Sale',
  ),
];
