<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class House extends Model
{
    use HasFactory;

    protected $guarded = [];


    public function region()
    {
        return $this->belongsTo('App\Models\Region');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function owners()
    {
        return $this->belongsToMany('App\Models\Owner', 'house_owners', 'house_id', 'owner_id');
    }

    public function images()
    {
        return $this->hasMany('App\Models\Image');
    }

    public function userslikes()
    {
        return $this->belongsToMany('App\Models\User', 'likes', 'house_id', 'user_id');
    }

    public function userscomments()
    {
        return $this->belongsToMany('App\Models\User', 'comments', 'house_id', 'user_id');
    }

}
