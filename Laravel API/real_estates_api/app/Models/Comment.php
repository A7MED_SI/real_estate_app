<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function replies()
    {
        return $this->hasMany('App\Models\Reply');
    }

    public function house()
    {
        return $this->belongsTo('App\Models\House');
    }
    
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
