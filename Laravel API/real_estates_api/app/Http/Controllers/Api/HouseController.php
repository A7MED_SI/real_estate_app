<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\House;
use Illuminate\Http\Request;
use App\Http\Resources\HomeHouseResource;
use App\Models\Region;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class HouseController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $houses = House::orderBy('updated_at', 'desc')->paginate(5);
        $collection = $houses->getCollection();
        return HomeHouseResource::collection($collection)->additional(['lastPage' => $houses->lastPage()]);
    }

    public function indexMostLike()
    {
        $houses = House::withCount('userslikes')
        ->orderBy('userslikes_count', 'desc')->paginate(5);
        $collection = $houses->getCollection();
        return HomeHouseResource::collection($collection)->additional(['lastPage' => $houses->lastPage()]);
    }

    public function indexUserLikesHouses()
    {
        $user = auth('api')->user();

        $houses = $user->houseslikes()->paginate(5);
        $collection = $houses->getCollection();
        return HomeHouseResource::collection($collection)->additional(['lastPage' => $houses->lastPage()]);
    }

    public function indexUserHouses()
    {
        $user = auth('api')->user();

        $houses = $user->houses()->get();
        return HomeHouseResource::collection($houses);
    }

    public function indexRegionHouse(Region $region)
    {
        $houses = $region->houses()
            ->orderBy('updated_at')->paginate(5);


        return HomeHouseResource::collection($houses);
    }

    public function indexNearestHouse()
    {
        $lattitude = request()->lattitude;
        $longitude = request()->longitude;
        $houses = House::selectRaw('*, ( 6367 * acos( cos( radians(' . $lattitude . ') ) * cos( radians( lattitude ) ) * cos( radians( longitude ) - radians(' . $longitude . ') ) + sin( radians(' . $lattitude . ') ) * sin( radians( lattitude ) ) ) ) AS distance')
            ->having('distance', '<', 2)
            ->orderBy('distance')
            ->get();
        $paginated = $this->paginate($houses, $perPage = 5, $page = null, $options = [])->each(function ($item, $key) {
            if (
                $key == 'data'
            )
                return  $item;
        });
        return HomeHouseResource::collection($paginated);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Region $region)
    {
        // dd(json_encode(request()->other_image), true);   
        $user = auth('api')->user();

        $data = request()->validate([
            'description' => 'nullable',
            'street_address' => 'nullable',
            'price' => 'nullable',
            'bedrooms' => 'nullable',
            'bathrooms' => 'nullable',
            'sqft_living' => 'nullable',
            'sqft_lot' => 'nullable',
            'floors' => 'nullable',
            'waterfront' => 'nullable',
            'view' => 'nullable',
            'condition' => 'nullable',
            'grade' => 'nullable',
            'sqft_above' => 'nullable',
            'sqft_basement' => 'nullable',
            'yr_built' => 'nullable',
            'yr_renovated' => 'nullable',
            'zipcode' => 'nullable',
            'lattitude' => 'nullable',
            'longitude' => 'nullable',
        ]);
        // if (is_null(request()->base_image)) {
        $data['base_image'] = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQVtpH5sHT4C88ozzCw54HgjWxxVMM4UqHCqg&usqp=CAU';
        // }

        // $data['other_image'] = json_encode(request()->other_image);
        // if (is_null(request()->other_image)) {
        $data['other_image'] = json_encode([
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTkqwpSViG_QQM93Lr1IkE-QuiF6Chl415BCA&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRmGGSPZ2uJ0EDMyQXTe0V_tp2VPOYn7kuNvw&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQwZyRqfv_TqFhdMzao4fhdtv6SfDkViHS7SA&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSdTNNdXolTqU5S_unkEXy8w5qNuBuqfooNvA&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRTuu7JsC5vleKDBllXaEMYJphaRZtXwqFliQ&usqp=CAU',
        ]);
        // }

        $data['user_id'] = $user->id;
        $data['region_id'] = $region->id;
        $house = House::create($data);

        return new HomeHouseResource($house);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\House  $house
     * @return \Illuminate\Http\Response
     */
    public function show(House $house)
    {
        return new HomeHouseResource($house);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\House  $house
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, House $house)
    {
        $data = request()->validate([
            'description' => 'nullable',
            'street_address' => '',
            'base_image' => '',
            'price' => '',
            'bedrooms' => '',
            'bathrooms' => '',
            'sqft_living' => '',
            'sqft_lot' => '',
            'floors' => '',
            'waterfront' => '',
            'view' => '',
            'condition' => '',
            'grade' => '',
            'sqft_above' => '',
            'sqft_basement' => '',
            'yr_built' => '',
            'yr_renovated' => '',
            'zipcode' => '',
            'lattitude' => '',
            'longitude' => '',
        ]);
        $data['other_image'] = json_encode(request()->other_image);

        $house->update($data);


        return new HomeHouseResource($house);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\House  $house
     * @return \Illuminate\Http\Response
     */
    public function destroy(House $house)
    {

        $house->delete();
    }



    public function paginate($items, $perPage = 15, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);

        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
}
