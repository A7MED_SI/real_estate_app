<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Like;
use App\Models\House;
use Illuminate\Http\Request;

class LikeController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, House $house)
    {
        $user = auth('api')->user();

        Like::create([
            'user_id'  => $user->id,
            'house_id'  => $house->id,
        ]);
        return response()->json('This Offer Is Add To Your Favorite');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Like  $like
     * @return \Illuminate\Http\Response
     */
    public function destroy(House $house)
    {

        $user = auth('api')->user();

        $like = Like::where('user_id', $user->id)->where('house_id', $house->id)->firstOrFail();

        $like->delete();
        return response()->json('the House Is deleted From Your Favorite');
    }
}
