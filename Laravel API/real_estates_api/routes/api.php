<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group([
    'prefix' => 'auth'
], function ($router) {
    Route::post('register', [App\Http\Controllers\Api\UserController::class, 'register']);
    Route::post('login', [App\Http\Controllers\Api\UserController::class, 'login']);

    Route::group([
        'middleware' => 'auth:api'
    ], function () {
        Route::get('details', [App\Http\Controllers\Api\UserController::class, 'details']);
        Route::get('logout',  [App\Http\Controllers\Api\UserController::class, 'logout']);
        Route::get('user',  [App\Http\Controllers\Api\UserController::class, 'user']);
    });
});




Route::group([], function ($router) {


    Route::group([
        'middleware' => 'auth:api'
    ], function () {

        //House
        Route::apiResource('house', App\Http\Controllers\Api\HouseController::class)->except('store');
        Route::GET('{region}/house', [App\Http\Controllers\Api\HouseController::class, 'indexRegionHouse']);
        Route::GET('userLikeshouse', [App\Http\Controllers\Api\HouseController::class, 'indexUserLikesHouses']);
        Route::GET('userhouse', [App\Http\Controllers\Api\HouseController::class, 'indexUserHouses']);
        Route::GET('mostLikehouse', [App\Http\Controllers\Api\HouseController::class, 'indexMostLike']);
        Route::POST('nearesthouse', [App\Http\Controllers\Api\HouseController::class, 'indexNearestHouse']);
        Route::POST('{region}/house', [App\Http\Controllers\Api\HouseController::class, 'store']);

        //Region
        Route::GET('region', [App\Http\Controllers\Api\RegionController::class, 'index']);
        //Like
        Route::POST('like/{house}', [App\Http\Controllers\Api\LikeController::class, 'store']);
        Route::DELETE('dislike/{house}', [App\Http\Controllers\Api\LikeController::class, 'destroy']);

        //Comment
        Route::GET('comment/{house}', [App\Http\Controllers\Api\CommentController::class, 'index']);
        Route::POST('comment/{house}', [App\Http\Controllers\Api\CommentController::class, 'store']);

        //Reply
        Route::GET('reply/{comment}', [App\Http\Controllers\Api\ReplyController::class, 'index']);
        Route::POST('reply/{comment}', [App\Http\Controllers\Api\ReplyController::class, 'store']);

    });
});
