<?php

namespace Database\Factories;

use App\Models\HouseOwner;
use Illuminate\Database\Eloquent\Factories\Factory;

class HouseOwnerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = HouseOwner::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'house_id'    => \App\Models\House::all()->random()->id,
            'owner_id'    => \App\Models\Owner::all()->random()->id
        ];
    }
}
